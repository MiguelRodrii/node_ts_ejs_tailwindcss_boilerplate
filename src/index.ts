import express, { Application } from 'express';
import {routes} from "./routes/index";

const app: Application = express();
const PORT = process.env.PORT || 8000;

app.set("view engine", "ejs")

app.use(express.static("public"));

app.use(routes);

app.listen(PORT, (): void => {
    console.log(`Server Running here 👉 http://localhost:${PORT}`);
});