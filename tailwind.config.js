module.exports = {
  purge: [
    './public/**/*.html',
    './views/**/*.ejs',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      olive: {
        darkest: '#54FC51',
        dark: '#80E048',
        DEFAULT: '#CBF75C',
        light: '#E0DE48',
        lightest: '#FCE751',
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
